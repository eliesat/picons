#!/bin/sh

# Remove unnecessary files and folders
###########################################
[ -d "/CONTROL" ] && rm -r /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1

# Check for mounted storage
###########################################
echo "> Checking mounted storage, please wait..."
sleep 2
for ms in "/media/hdd" "/media/usb" "/media/mmc" "/usr/share/enigma2" 
do
    if mount|grep $ms >/dev/null 2>&1; then
    echo "> Mounted storage found at: $ms"
    pp=$ms/Ajpanel_Eliesatpanel/
    pp1=/Ajpanel_Eliesatpanel/
    break
    fi
done
sleep 1

# Create picon directory if do not exists
###########################################
if [ ! -d "$ms/picon" ]; then
    mkdir -p $ms/picon
fi

# Download and install picons
###########################################
echo ""
echo "> Downloading & installing picons please wait..."
sleep 5
plugin="picons"
version="7w"
url="https://gitlab.com/eliesat/picons/-/raw/main/$plugin-$version.tar.gz"
package="$ms/picon/$plugin-$version.tar.gz"
wget --show-progress -qO $package --no-check-certificate $url
tar -xzf $package -C $ms/picon
extract=$?
rm -rf $package >/dev/null 2>&1

echo ""
if [ $extract -eq 0 ]; then
    echo "> $plugin-$version package installed successfully"
    sleep 3
else
    echo "> $plugin-$version package installation failed"
    sleep 3
fi
