#!/bin/sh

# Configuration
#########################################
plugin="picons"
version="07.12.2024"
targz_file="$plugin-$version.tar.gz"
url="https://dl.dropboxusercontent.com/scl/fi/njzq40rndpd08bx73k0l8/picons-07.12.2024.tar.gz?rlkey=srpuqnpidbdqxk62v4wjtq08p&st=5fr6yy1i&dl=0"
temp_dir="/tmp"

# Check for mounted storage
###########################################
echo "> Checking mounted storage, please wait..."
sleep 1
for ms in "/media/hdd"
do
    if mount|grep $ms >/dev/null 2>&1; then
    echo "> Mounted storage found at: $ms"
    break
    else
    echo "> Mount your extrenal storage at: $ms and try again"
    fi
done
sleep 1

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By ElieSat"
}
cleanup
    